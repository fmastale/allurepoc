import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.when;

public class ArticlesTest {

    @Test
    void getListOfArticlesCheckCorrectStatusCode() {
        when().get("https://conduit.productionready.io/api/articles").then().statusCode(200);
    }
}
